$(document).ready(function () {
    var vendas = $('#vendas').val();
    vendas = vendas.split(',');

    var custos = $('#custos').val();
    custos = custos.split(',');

    var contexto = $('#grafico')[0].getContext('2d');
    var grafico = new Chart(contexto, {
        type: 'line',
        data: {
            labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio'],
            datasets: [
                {
                    label: 'Vendas',
                    backgroundColor: '#FF0000',
                    borderColor: '#FF0000',
                    data: vendas,
                    fill: false
                },
                {
                    label: 'Custos',
                    backgroundColor: '#00FF00',
                    borderColor: '#00FF00',
                    data: custos,
                    fill: false
                }
            ]
        }
    });
});
