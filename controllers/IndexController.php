<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 16:50
 */

namespace controllers;

use core\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $vendas = array(10, 20, 30, 40, 20);
        $custos = array(8, 15, 37, 97, 35);

        $dados['vendas'] = implode(', ', $vendas);
        $dados['custos'] = implode(', ', $custos);

        $this->loadTemplate('index/index', $dados);
    }

}
