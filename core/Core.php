<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 15:30
 */

namespace core;


class Core
{
    public function run()
    {
        $url = '/';

        if (isset($_GET['url'])){
            $url = $_GET['url'];
        }

        $params = [];
        if ($url != '/') {
            $url = explode('/', $url);
            array_shift($url);

            $currentController = '\controllers\\' . ucfirst($url[0]).'Controller';
            array_shift($url);

            if (isset($url) && !empty($url[0])){
                $currentAction = $url[0];
                array_shift($url);
            }else{
                $currentAction = 'index';
            }

            if (count($url) > 0){
                $params = $url;
            }


        }else {
            $currentController = '\controllers\IndexController';
            $currentAction = 'index';
        }

        $c = new $currentController();
        call_user_func_array([$c,$currentAction], $params);

    }
}