<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 17:42
 */

namespace core;


class Controller
{

    public function loadView($viewName, $viewData = array()): void
    {
        extract($viewData);
        require_once('views/'.$viewName.'.php');
    }

    public function loadTemplate($viewName, $viewData = array()): void
    {
        require_once 'views/template.php';
    }

    public function loadCache($viewName, $viewData = array()): void
    {
        $cache = new Cache();

        if ($cache->viewExists($viewName) && $cache->isValid($viewName)){
            $cache->loadViewCache($viewName);
            exit;
        }

        ob_start();
        $this->loadTemplate($viewName, $viewData);
        $html = ob_get_contents();
        ob_end_clean();

        $cache->saveViewCache($viewName, $html);

        echo $html;
    }

}