<html>
    <head>
        <title>Meu site</title>
        <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/style.css">
    </head>
    <body>
    	
        <?php $this->loadView($viewName, $viewData); ?>
        <script type="text/javascript" src="<?=BASE_URL?>assets/js/chart.min.js"></script>
        <script type="text/javascript" src="<?=BASE_URL?>assets/js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="<?=BASE_URL?>assets/js/custom.js"></script>
    </body>
</html>